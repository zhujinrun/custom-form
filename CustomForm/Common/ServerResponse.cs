﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace CustomForm.Common
{
    public class ServerResponse
    {
        private static IActionResult BuildResult(int code, string msg, object data)
        {
            return new JsonResult(new { code, msg, data });
        }

        public static IActionResult Yes(object data, string msg = "success")
        {
            return BuildResult(0, msg, data);
        }

        public static IActionResult Yes(string msg = "success")
        {
            return BuildResult(0, msg, null);
        }

        public static IActionResult No(object data, string msg = "failure")
        {
            return BuildResult(1, msg, data);
        }

        public static IActionResult No(string msg = "failure")
        {
            return BuildResult(1, msg, null);
        }
    }
}
