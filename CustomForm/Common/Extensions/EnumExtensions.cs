﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace CustomForm.Common.Extensions
{
    public static class EnumExtensions
    {
        public static DisplayAttribute GetDisplayAttribute(this Enum @enum)
        {
            var member = @enum.GetType().GetMember(@enum.ToString()).FirstOrDefault();
            return (DisplayAttribute)member.GetCustomAttribute(typeof(DisplayAttribute));
        }
    }
}
