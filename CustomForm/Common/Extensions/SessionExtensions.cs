﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace CustomForm.Common.Extensions
{
    public static class SessionExtensions
    {
        public static void Set<T>(this ISession session, string key, T value)
        {
            session.SetString(key, JsonConvert.SerializeObject(value));
        }

        public static T Get<T>(this ISession session, string key)
        {
            var value = session.GetString(key);
            return value == null ? default(T) : JsonConvert.DeserializeObject<T>(value);
        }
    }

    public class AdminSession
    {
        public long adminid { get; set; }
        public string adminuser { get; set; }
        public string adminqx { get; set; }

        public int oaid { get; set; }
        public string danwei { get; set; }
    }
}
