﻿using CustomForm.Entities;
using System.ComponentModel.DataAnnotations;

namespace CustomForm.Common
{
    /// <summary>
    /// [Name]      - Folder Name
    /// [GroupName] - Table Name
    /// [ShortName] - Column Name
    /// </summary>
    public enum UploadType
    {
        [Display(Name = "touxiang", GroupName = nameof(User), ShortName = "touxiang", Description = "User Avatar")]
        Avatar = 1,
    }
}
