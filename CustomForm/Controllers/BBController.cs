﻿using CustomForm.Entities;
using CustomForm.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace CustomForm.Controllers
{
    public class BBController : BaseController
    {
        private readonly IFreeSql _sql;

        public BBController(IFreeSql sql)
        {
            _sql = sql;
        }

        [HttpGet]
        public IActionResult All(int page = 1, int size = 10)
        {
            var list = _sql.Select<BB>().From<BB_flpx, User>((s, b, c) => s
             .LeftJoin(a => a.fbfl == b.id)
             .LeftJoin(a => a.userid == c.id))
            .OrderByDescending((a, b, c) => a.id)
            .Count(out var count)
            .Page(page, size)
            .ToList((a, b, c) => new BBItemViewModel
            {
                id = a.id,
                userid = a.userid,
                fbsj = a.fbsj,
                fbfl = a.fbfl,
                fbbt = a.fbbt,
                flmc = b.flmc,
                username = c.username
            });

            var model = new BBViewModel() { BBList = list, BBCount = count };
            return View(model);
        }

        [HttpGet]
        public IActionResult My(int page = 1, int size = 10)
        {
            var list = _sql.Select<BB>().From<BB_flpx, User>((s, b, c) => s
             .LeftJoin(a => a.fbfl == b.id)
             .LeftJoin(a => a.userid == c.id))
             .Where((a, b, c) => a.userid == Admin.adminid)
            .OrderByDescending((a, b, c) => a.id)
            .Count(out var count)
            .Page(page, size)
            .ToList((a, b, c) => new BBItemViewModel
            {
                id = a.id,
                userid = a.userid,
                fbsj = a.fbsj,
                fbfl = a.fbfl,
                fbbt = a.fbbt,
                flmc = b.flmc,
                username = c.username
            });

            var model = new BBViewModel() { BBList = list, BBCount = count };
            return View(model);
        }

        [HttpGet]
        public IActionResult Show(long id)
        {
            var model = _sql.Select<BB>().From<BB_flpx, User>((s, b, c) => s
             .LeftJoin(a => a.fbfl == b.id)
             .LeftJoin(a => a.userid == c.id))
            .Where((a, b, c) => a.id == id)
            .ToOne((a, b, c) => new BBItemViewModel
            {
                id = a.id,
                userid = a.userid,
                fbsj = a.fbsj,
                fbfl = a.fbfl,
                fbbt = a.fbbt,
                flmc = b.flmc,
                username = c.username
            });

            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> Form(long id = 0)
        {
            var categories = await _sql.Select<BB_flpx>().OrderByDescending(c => c.flpx).ToListAsync();
            ViewBag.Categories = categories;

            var form = new BBFormViewModel() { id = id, fbfl = 1 };
            if (id > 0)
            {
                var bb = await _sql.Select<BB>(new { id }).ToOneAsync();
                form = bb == null ? form : new BBFormViewModel() { id = bb.id, fbbt = bb.fbbt, fbfl = bb.fbfl, fblr = bb.fblr };
            }
            return View(form);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Form(BBFormViewModel form)
        {
            if (ModelState.IsValid)
            {
                var bb = new BB
                {
                    fbsj = DateTime.Now,
                    userid = Admin.adminid
                };

                if (form.id > 0)
                {
                    bb = await _sql.Select<BB>(new { form.id }).ToOneAsync();
                }

                bb.fbfl = form.fbfl;
                bb.fbbt = form.fbbt;
                bb.fblr = form.fblr;

                await _sql.InsertOrUpdate<BB>().SetSource(bb).ExecuteAffrowsAsync();
                return RedirectToAction(nameof(My));
            }

            var categories = await _sql.Select<BB_flpx>().OrderByDescending(c => c.flpx).ToListAsync();
            ViewBag.Categories = categories;

            return View(form);
        }

        [HttpGet]
        public async Task<IActionResult> Del(long id)
        {
            await _sql.Delete<BB>(new { id }).ExecuteAffrowsAsync();
            return RedirectToAction(nameof(My));
        }


        [HttpGet]
        public async Task<IActionResult> CateList(int page = 1, int size = 10)
        {
            var list = await _sql.Select<BB_flpx>().OrderByDescending(c => c.flpx).Count(out var count).Page(page, size).ToListAsync();
            var model = new BBCateViewModel() { CateList = list, ListCount = count };
            return View(model);
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> CateForm(long id, BBCateFormViewModel model)
        {
            var source = new BB_flpx() { id = id, flpx = model.flpx, flmc = model.flmc };
            await _sql.InsertOrUpdate<BB_flpx>().SetSource(source).ExecuteAffrowsAsync();
            return RedirectToAction(nameof(CateList));
        }

        [HttpGet]
        public async Task<IActionResult> CateDel(long id)
        {
            await _sql.Delete<BB_flpx>(new { id }).ExecuteAffrowsAsync();
            return RedirectToAction(nameof(CateList));
        }

    }
}
