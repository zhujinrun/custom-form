﻿using CustomForm.Common.Extensions;
using CustomForm.Entities;
using CustomForm.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace CustomForm.Controllers
{
    public class UserController : BaseController
    {
        private readonly IFreeSql _sql;

        public UserController(IFreeSql sql)
        {
            _sql = sql;
        }

        [HttpGet]
        public async Task<IActionResult> List(int page = 1, int size = 10)
        {
            var list = await _sql.Select<User>().From<User>((s, b) => s.LeftJoin(a => a.pid == b.id))
            .Count(out var count).Page(page, size)
            .ToListAsync((a, b) => new UserItemViewModel
            {
                id = a.id,
                username = a.username,
                pid = a.pid,
                pname = b.username,
                quanxian = a.quanxian,
                danwei = a.danwei,
                xingming = a.xingming,
                dianhua = a.dianhua,
                email = a.email,
                zhucesj = a.zhucesj
            });

            var model = new UserViewModel() { UserList = list, ListCount = count };
            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> Form(long id)
        {
            var model = new UserFormViewModel() { id = 0, oaid = Admin.oaid, danwei = Admin.danwei };

            if (id > 0)
            {
                model = await _sql.Select<User>().Where(a => a.id == id)
                  .ToOneAsync(a => new UserFormViewModel
                  {
                      id = a.id,
                      oaid = a.oaid,
                      pid = a.pid,
                      danwei = a.danwei,
                      xingming = a.xingming,
                      touxiang = a.touxiang,
                      dianhua = a.dianhua,
                      email = a.email,
                      remark = a.remark
                  });
            }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Form(UserFormViewModel model)
        {
            if (ModelState.IsValid)
            {
                var source = new User()
                {
                    id = model.id,
                    touxiang = 0,
                    quanxian = "管理",
                    username = model.username,
                    password = model.password.MD5(true),
                    zhucesj = DateTime.Now
                };

                if (model.id > 0)
                {
                    source = await _sql.Select<User>(new { id = model.id }).ToOneAsync();
                }

                source.oaid = model.oaid;
                source.pid = model.pid;
                source.danwei = model.danwei;
                source.xingming = model.xingming;
                source.dianhua = model.dianhua;
                source.email = model.email;
                source.remark = model.remark;

                await _sql.InsertOrUpdate<User>().SetSource(source).ExecuteAffrowsAsync();
                return RedirectToAction(nameof(List));
            }

            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> Del(int id)
        {
            await _sql.Delete<User>(new { id }).ExecuteAffrowsAsync();
            return RedirectToAction(nameof(List));
        }
    }
}
