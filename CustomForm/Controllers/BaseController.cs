﻿using CustomForm.Common.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace CustomForm.Controllers
{
    public class BaseController : Controller
    {
        public AdminSession Admin { get; private set; }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
#if DEBUG
            Admin = new AdminSession() { adminid = 1, oaid = 10001, adminuser = "admin", adminqx = "超管", danwei = "汉台区老干部活动中心" };
            context.HttpContext.Session.Set(nameof(AdminSession), Admin);
#else
            Admin = context.HttpContext.Session.Get<AdminSession>(nameof(AdminSession));
            if (Admin == null)
            {
                context.Result = new RedirectResult("/Login");
                return;
            }
#endif
        }
    }
}
