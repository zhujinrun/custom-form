﻿using CustomForm.Common.Extensions;
using CustomForm.Entities;
using CustomForm.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace CustomForm.Controllers
{
    public class HomeController : BaseController
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IFreeSql _sql;

        public HomeController(ILogger<HomeController> logger, IFreeSql sql)
        {
            _logger = logger;
            _sql = sql;
        }

        [HttpGet]
        public async Task<IActionResult> Index(int page = 1, int size = 5)
        {
            var list = await _sql.Select<BB>().From<BB_flpx, User>((s, b, c) => s
             .LeftJoin(a => a.fbfl == b.id)
             .LeftJoin(a => a.userid == c.id))
            .OrderByDescending((a, b, c) => a.id)
            .Count(out var count)
            .Page(page, size)
            .ToListAsync((a, b, c) => new HomeBBViewModel
            {
                id = a.id,
                userid = a.userid,
                fbsj = a.fbsj,
                fbfl = a.fbfl,
                fbbt = a.fbbt,
                flmc = b.flmc,
                username = c.username
            });

            var model = new HomeViewModel() { BBList = list, BBListCount = count };
            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> SetUser()
        {
            var model = await _sql.Select<User>().Where(a => a.id == Admin.adminid)
               .ToOneAsync(a => new SetUserViewModel
               {
                   id = a.id,
                   oaid = a.oaid,
                   pid = a.pid,
                   danwei = a.danwei,
                   xingming = a.xingming,
                   touxiang = a.touxiang,
                   dianhua = a.dianhua,
                   email = a.email,
                   remark = a.remark
               });

            return View(model);
        }

        public async Task<IActionResult> SetUser(SetUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                var source = await _sql.Select<User>(new { id = model.id }).ToOneAsync();

                source.oaid = model.oaid;
                source.pid = model.pid;
                if (source.pid == Admin.adminid)
                {
                    source.pid = 0;
                }
                source.danwei = model.danwei;
                source.xingming = model.xingming;
                source.dianhua = model.dianhua;
                source.email = model.email;
                source.remark = model.remark;

                await _sql.InsertOrUpdate<User>().SetSource(source).ExecuteAffrowsAsync();
                return Redirect("~/");
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult SetPass()
        {
            return View(new SetPassViewModel());
        }

        [HttpPost]
        public async Task<IActionResult> SetPass(SetPassViewModel model)
        {
            if (ModelState.IsValid)
            {
                var source = await _sql.Select<User>(new { id = Admin.adminid }).ToOneAsync();
                if (model.pass0.MD5(true).Equals(source.password))
                {
                    if (model.pass1.Equals(model.pass2))
                    {
                        source.password = model.pass1.MD5(true);
                        await _sql.Update<User>().SetSource(source).ExecuteAffrowsAsync();
                    }
                }
                return Redirect("~/");
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Logout()
        {
            HttpContext.Session.Remove(nameof(AdminSession));
            HttpContext.Session.Clear();

            return Redirect("/Login");
        }
    }
}
