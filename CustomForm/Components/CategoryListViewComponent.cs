﻿using CustomForm.Entities;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.IO;
using System.Threading.Tasks;

namespace CustomForm.Components
{
    /// <summary>
    /// 视图组件
    /// </summary>
    [ViewComponent(Name = "CategoryList")]
    public class CategoryListViewComponent : ViewComponent
    {
        private readonly IFreeSql _sql;

        public CategoryListViewComponent(IFreeSql sql)
        {
            _sql = sql;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var items = await _sql.Select<BB_flpx>().ToListAsync();
            return View(items);
        }
    }
}
