﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CustomForm.Models
{
    public class HomeViewModel
    {
        public List<HomeBBViewModel> BBList { get; set; }
        public long BBListCount { get; set; }
    }

    public class HomeBBViewModel
    {
        public long id { get; set; }
        public long userid { get; set; }

        public DateTime fbsj { get; set; }
        public int fbfl { get; set; }
        public string fblr { get; set; }
        public string fbbt { get; set; }

        public string flmc { get; set; }
        public string username { get; set; }
    }

    public class SetUserViewModel
    {
        public long id { get; set; }

        public int oaid { get; set; }
        public long pid { get; set; }

        public string danwei { get; set; }
        public string xingming { get; set; }
        public long touxiang { get; set; }

        public string quanxian { get; set; }

        public string dianhua { get; set; }
        public string email { get; set; }
        public string remark { get; set; }
    }

    public class SetPassViewModel
    {
        [Required]
        public string pass0 { get; set; }
        [Required]
        public string pass1 { get; set; }
        [Required]
        public string pass2 { get; set; }
    }
}
