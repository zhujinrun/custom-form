﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CustomForm.Models
{
    public class UserViewModel
    {
        public List<UserItemViewModel> UserList { get; set; }
        public long ListCount { get; set; }
    }

    public class UserItemViewModel
    {
        public long id { get; set; }
        public long pid { get; set; }
        public string pname { get; set; }
        public string username { get; set; }
        public string quanxian { get; set; }
        public string danwei { get; set; }
        public string xingming { get; set; }
        public string dianhua { get; set; }
        public string email { get; set; }
        public DateTime zhucesj { get; set; }
    }

    public class UserFormViewModel
    {
        public long id { get; set; }

        public int oaid { get; set; }
        public long pid { get; set; }

        public string username { get; set; }
        public string password { get; set; }

        public string danwei { get; set; }
        public string xingming { get; set; }
        public long touxiang { get; set; }

        public string quanxian { get; set; }

        public string dianhua { get; set; }
        public string email { get; set; }
        public string remark { get; set; }
    }
}
