﻿using CustomForm.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CustomForm.Models
{
    public class BBViewModel
    {
        public List<BBItemViewModel> BBList { get; set; }
        public long BBCount { get; set; }
    }

    public class BBItemViewModel
    {
        public long id { get; set; }
        public long userid { get; set; }

        public DateTime fbsj { get; set; }
        public int fbfl { get; set; }
        public string fblr { get; set; }
        public string fbbt { get; set; }

        public string flmc { get; set; }
        public string username { get; set; }
    }

    public class BBFormViewModel
    {
        public long id { get; set; }

        [Required(ErrorMessage = "发布分类不能为空")]
        public int fbfl { get; set; }

        [Required(ErrorMessage = "发布标题不能为空")]
        public string fbbt { get; set; }

        [Required(ErrorMessage = "发布内容不能为空")]
        public string fblr { get; set; }

    }

    public class BBCateViewModel
    {
        public List<BB_flpx> CateList { get; set; }
        public long ListCount { get; set; }
    }

    public class BBCateFormViewModel
    {
        public long id { get; set; }

        [Required(ErrorMessage = "分类排序不能为空")]
        public int flpx { get; set; }

        [Required(ErrorMessage = "分类名称不能为空")]
        public string flmc { get; set; }
    }
}
