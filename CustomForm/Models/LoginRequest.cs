﻿using System.ComponentModel.DataAnnotations;

namespace CustomForm.Models
{
    public class LoginRequest
    {
        [Required(ErrorMessage = "请输入用户名")]
        public string adminuser { get; set; }

        [Required(ErrorMessage = "请输入密 码")]
        public string adminpass { get; set; }
    }
}
