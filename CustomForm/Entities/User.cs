﻿using System;
using FreeSql.DataAnnotations;

namespace CustomForm.Entities
{

    public class User
    {
        [Column(IsIdentity = true)]
        public long id { get; set; }
        public int oaid { get; set; }
        public long pid { get; set; }

        public string weixin { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string quanxian { get; set; }
        public string danwei { get; set; }
        public string xingming { get; set; }
        public long touxiang { get; set; }
        public string dianhua { get; set; }
        public string email { get; set; }
        public string remark { get; set; }
        public DateTime zhucesj { get; set; }
    }

    public class BB
    {
        [Column(IsIdentity = true)]
        public long id { get; set; }
        public long userid { get; set; }

        public DateTime fbsj { get; set; }
        public int fbfl { get; set; }
        public string fblr { get; set; }
        public string fbbt { get; set; }
    }

    public class BB_flpx
    {
        [Column(IsIdentity = true)]
        public long id { get; set; }

        public int flpx { get; set; }
        public string flmc { get; set; }
    }

    public class Caozuo
    {
        [Column(IsIdentity = true)]
        public long id { get; set; }

        public string un { get; set; }
        public string cz { get; set; }
        public DateTime sj { get; set; }
    }

    public class Form1
    {
        [Column(IsIdentity = true)]
        public long id { get; set; }
        public long userid { get; set; }

        public DateTime fbsj { get; set; }
        public string fbfl { get; set; }
        public string fbfl2 { get; set; }
        public string fblr { get; set; }
        public string fbbt { get; set; }

        public int fbts { get; set; }
        public int fbym { get; set; }
        public int sj { get; set; }
    }

    public class Form2
    {
        [Column(IsIdentity = true)]
        public long id { get; set; }
        public long userid { get; set; }

        public int tbid { get; set; }
        public string tnbr { get; set; }
        public DateTime fbsj { get; set; }
        public string czry { get; set; }
        public string qianzi { get; set; }
        public int shenhe { get; set; }
    }

    public class Wenjian
    {
        [Column(IsIdentity = true)]
        public long id { get; set; }
        public string ysmc { get; set; }
        public string wjlj { get; set; }
    }
}
